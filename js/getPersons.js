const url="http://localhost:3000/persons"

function fillPersonsTable(persons) {
  if(!persons || persons.length===0) return;
  let table="<tr>"
  for(let key of Object.keys(persons[0])) {
    table += `<th>${key}</th>`
  }
  table+="</tr>\n";
  for(let person of persons){
    table+="<tr>"
    for(let key of Object.keys(persons[0])) {
      table += `<td>${person[key]}</td>`
    }
    table+="</tr>\n";
  }
  document.getElementById("persons-table").innerHTML=table;
}

function showPersons() {
  getPersons().then(fillPersonsTable)
  
}
 
 function getPersons(){
  return fetch(url)
    .then(res => res.json())
 }
 
 showPersons()
